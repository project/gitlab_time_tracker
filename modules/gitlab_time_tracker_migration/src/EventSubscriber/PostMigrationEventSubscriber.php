<?php

namespace Drupal\gitlab_time_tracker_migration\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\gitlab_time_tracker\GitlabMutationsClient;
use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\node\NodeInterface;

/**
 * Class PostMigrationEventSubscriber.
 */
class PostMigrationEventSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\gitlab_time_tracker\GitlabMutationsClient definition.
   *
   * @var \Drupal\gitlab_time_tracker\GitlabMutationsClient
   */
  protected $gitlabTimeTrackerGitlabMutations;

  /**
   * Constructs a new PostMigrationEventSubscriber object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, GitlabMutationsClient $gitlab_time_tracker_gitlab_mutations) {
    $this->entityTypeManager = $entity_type_manager;
    $this->gitlabTimeTrackerGitlabMutations = $gitlab_time_tracker_gitlab_mutations;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['migrate.post_import'] = ['migratePostImport'];

    return $events;
  }

  /**
   * This method i called when the migrate.post_import is dispatched.
   *
   * @param \Drupal\migrate\Event\MigrateImportEvent $event
   *   The dispatched event.
   */
  public function migratePostImport(Event $event) {
    if (
      $event instanceof MigrateImportEvent &&
      $event->getMigration()->getPluginId() === 'gitlab_time_tracker_node_timetrack'
    ) {
      $source = $event->getMigration()->getSourceConfiguration();

      $issue_id = $source['issue_id'];
      $project_id = $source['project_id'];

      $issues = $this->entityTypeManager->getStorage('node')->getQuery()
        ->condition('type', 'issue')
        ->condition('field_project.entity.field_gitlab_id', $project_id)
        ->condition('field_gitlab_iid', $issue_id)
        ->range(0, 1)
        ->execute();

      if (is_array($issues)) {
        $issue_nid = reset($issues);
        $issue = $this->entityTypeManager->getStorage('node')->load($issue_nid);
      }

      if ($issue) {
        $this->updateIssueTimeTracks($issue);
      }

    }
  }

  /**
   *
   */
  protected function updateIssueTimeTracks(NodeInterface $issue) {
    $issue_id = $issue->id();

    $timetracks = $this->entityTypeManager->getStorage('node')->loadByProperties(
      [
        'type' => 'timetrack',
        'field_issue' => $issue_id,
      ]
    );

    $sum = array_sum(
      array_map(
        function ($node) {
          return $node->field_time_spent->value;
        },
        $timetracks
      )
    );

    if ($sum !== $issue->field_time_spent->value) {
      $issue->field_time_spent = $sum;
      $issue->save();

      $sum = round($sum / 60, 2) . 'h';

      $this->gitlabTimeTrackerGitlabMutations->updateIssueTime(
        $issue->field_project->entity->field_gitlab_id->value,
        $issue->field_gitlab_iid->value,
        $sum
      );
    }
  }

}
