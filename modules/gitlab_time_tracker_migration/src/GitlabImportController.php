<?php

namespace Drupal\gitlab_time_tracker_migration;

use Drupal\gitlab_time_tracker\GitlabClientInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessage;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\migrate\Plugin\migrate\id_map\Sql;
use Drupal\migrate\Plugin\MigrateIdMapInterface;

/**
 * Class GitlabImportController.
 */
class GitlabImportController implements GitlabImportControllerInterface {

  /**
   * Drupal\gitlab_time_tracker_migration\GitlabClientInterface definition.
   *
   * @var \Drupal\gitlab_time_tracker_migration\GitlabClientInterface
   */
  protected $timeTrackerImportGitlab;

  /**
   * Drupal\gitlab_time_tracker_migration\GitlabClientInterface definition.
   *
   * @var Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $migrationPluginManager;


  /**
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new GitlabImportController object.
   */
  public function __construct(GitlabClientInterface $gitlab_time_tracker_migration_gitlab, MigrationPluginManagerInterface $migration_plugin_manager, EntityTypeManagerInterface $entityTypeManager) {
    $this->timeTrackerImportGitlab = $gitlab_time_tracker_migration_gitlab;
    $this->migrationPluginManager = $migration_plugin_manager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Remove all failed entries from migration id_map.
   */
  protected function removeFailedImports(MigrateIdMapInterface $idMap) : void {
    if ($idMap instanceof Sql &&  $idMap->errorCount() > 0) {
      $idMap->getDatabase()->delete($idMap->mapTableName())
        ->condition('source_row_status', MigrateIdMapInterface::STATUS_FAILED)
        ->execute();
    }
  }

  /**
   * Migrate all projects.
   */
  public function migrateProjects($project_id = NULL, $update = FALSE) {
    if (is_numeric($project_id)) {
      $migration = $this->migrationPluginManager->createInstance(
      'gitlab_time_tracker_node_project',
      [
        'source' => [
          'plugin' => 'gitlab_project',
          'track_changes' => TRUE,
          'project_id' => $project_id,
        ],
      ]
      );
    }
    else {
      $migration = $this->migrationPluginManager->createInstance(
      'gitlab_time_tracker_node_project',
      []
      );
    }

    if ($update) {
      $migration->getIdMap()->prepareUpdate();
    }

    $migration->setStatus(MigrationInterface::STATUS_IDLE);
    $executable = new MigrateExecutable($migration, new MigrateMessage());
    $executable->import();
  }

  /**
   *
   */
  public function migrateUsers() {
    $migration = $this->migrationPluginManager->createInstance(
    'gitlab_time_tracker_user',
    []
    );

    $this->removeFailedImports($migration->getIdMap());

    $migration->setStatus(MigrationInterface::STATUS_IDLE);
    $executable = new MigrateExecutable($migration, new MigrateMessage());
    $executable->import();
  }

  /**
   *
   */
  public function migrateIssues($project_id = 0, $update = FALSE) {
    $migration = $this->migrationPluginManager->createInstance(
    'gitlab_time_tracker_node_issue',
    [
      'source' => [
        'plugin' => 'gitlab_issue',
        'track_changes' => TRUE,
        'project_id' => $project_id,
      ],
    ]
    );

    $this->removeFailedImports($migration->getIdMap());

    if ($update) {
      $migration->getIdMap()->prepareUpdate();
    }

    $migration->setStatus(MigrationInterface::STATUS_IDLE);
    $executable = new MigrateExecutable($migration, new MigrateMessage());
    $executable->import();

  }


  public function migrateMilestones($project_id = 0, $update = FALSE) {
    $migration = $this->migrationPluginManager->createInstance(
    'gitlab_time_tracker_node_milestone',
    [
      'source' => [
        'plugin' => 'gitlab_milestone',
        'track_changes' => TRUE,
        'project_id' => $project_id,
      ],
    ]
    );

    $this->removeFailedImports($migration->getIdMap());

    if ($update) {
      $migration->getIdMap()->prepareUpdate();
    }

    $migration->setStatus(MigrationInterface::STATUS_IDLE);
    $executable = new MigrateExecutable($migration, new MigrateMessage());
    $executable->import();
  }

  /**
   *
   */
  public function migrateTimeTrack($project_id = 0, $issue_id = 0, $update = FALSE) {
    $migration = $this->migrationPluginManager->createInstance(
    'gitlab_time_tracker_node_timetrack',
    [
      'source' => [
        'plugin' => 'gitlab_time_track',
        'track_changes' => TRUE,
        'project_id' => $project_id,
        'issue_id' => $issue_id,
      ],
    ]
    );
    $migration->setStatus(MigrationInterface::STATUS_IDLE);
    $this->removeFailedImports($migration->getIdMap());

    if ($update) {
      $migration->getIdMap()->prepareUpdate();
    }

    $executable = new MigrateExecutable($migration, new MigrateMessage());
    $executable->import();

    $this->removeOrphanedTimeTrack($project_id, $issue_id);
  }

  /**
   * Remove orphaned time tracks.
   */
  protected function removeOrphanedTimeTrack($project_id, $issue_id) {
    $existing_ids = array_map(
      function ($row) {
        return $row['id'];
      },
      $this->timeTrackerImportGitlab->fetchComments($project_id, $issue_id)
    );

    $repository = $this->entityTypeManager->getStorage('node');
    $parent_query = $repository->getQuery();

    // Parent node.
    $parent = $parent_query
      ->condition('field_gitlab_iid', $issue_id, '=')
      ->condition('field_project.entity.field_gitlab_id', $project_id, '=')
      ->condition('type', 'issue')
      ->execute();

    if (!empty($parent)) {
      $parent = $repository->load(reset($parent));
    }
    else {
      $parent = NULL;
    }

    if (!is_null($parent) && !empty($existing_ids)) {
      $child_query = $repository->getQuery();
      $children = $child_query
        ->condition('type', 'timetrack')
        ->condition('field_gitlab_id', $existing_ids, 'NOT IN')
        ->condition('field_issue.target_id', $parent->id(), '=')
        ->execute();

      if (!empty($children)) {
        $nodes_to_removal = $repository->loadMultiple($children);
        $repository->delete($nodes_to_removal);
      }
    }

  }

}
