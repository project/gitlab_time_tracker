<?php

namespace Drupal\gitlab_time_tracker_migration\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;

/**
 * Provides a 'TimeTrackerParser' migrate process plugin.
 *
 * @MigrateProcessPlugin(
 *  id = "time_tracker_parser"
 * )
 */
class TimeTrackerParser extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $time = 0;

    if (is_array($value)) {
      $value = reset($value);
    }

    if (strpos($value, ':clock') !== FALSE) {
      $comment = array_filter(
        array_map(
          function ($element) {
            return trim($element);
          },
          explode('|', $value)
        )
      );

      $dates = explode(' ', $comment[0]);

      foreach ($dates as $date_part) {
        $match = [];

        if (preg_match_all('/(\d+,\d+|\d+\.\d+|\d+) *(h|m|d|w|min)/', $date_part, $match)) {
          $value = (float) str_replace(",", ".", $match[1][0]);
          switch ($match[2][0]) {
            case 'h':
              $time += $value * 60;
              break;

            case 'm':
            case 'min':
              $time += $value;
              break;

            case 'd':
              $time += $value * 60 * 24;
              break;

            case 'w':
              $time += $value * 60 * 24 * 7;
              break;
          }
        }

      }

      return $time;
    }
    else {
      throw new MigrateSkipRowException();
    }
  }
}
