<?php

namespace Drupal\gitlab_time_tracker_migration\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;

/**
 * Provides a 'GitlabTimeSpent' migrate process plugin.
 *
 * @MigrateProcessPlugin(
 *  id = "gitlab_time_spent"
 * )
 */
class GitlabTimeSpent extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_array($value)) {
      $value = reset($value);
    }

    return $value / 60;
  }
}
