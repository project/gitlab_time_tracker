<?php

namespace Drupal\gitlab_time_tracker_migration\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;

/**
 * Provides a 'GitlabRangeDate' migrate process plugin.
 *
 * @MigrateProcessPlugin(
 *  id = "gitlab_time_tracker_date_range"
 * )
 */
class GitlabRangeDate extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (is_array($value)) {
      $value = reset($value);
    }

    if (!empty($value)) {
      return $value;
    }
    elseif ($row->get('due_date')) {
      return $row->get('due_date');
    }
    elseif ($row->get('start_date')) {
      return $row->get('start_date');
    }
  }
}
