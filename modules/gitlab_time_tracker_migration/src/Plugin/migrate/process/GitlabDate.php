<?php

namespace Drupal\gitlab_time_tracker_migration\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\migrate\MigrateSkipRowException;

/**
 * Provides a 'GitlabDate' migrate process plugin.
 *
 * @MigrateProcessPlugin(
 *  id = "gitlab_time_tracker_date"
 * )
 */
class GitlabDate extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $time = 0;

    if (is_array($value)) {
      $value = reset($value);
    }


    // Removed decimal parts. It's problematic
    $value = preg_replace(
      '/\.\d\d\d/',
      '',
      $value
    );

    // Get format from migration configuration.
    if (!empty($this->configuration['format'])) {
      $format = $this->configuration['format'];
    }
    else {
      $format = "Y-m-d\TH:i:s";
    }

    $time = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $value, new \DateTimeZone('UTC'));

    if ($time) {
      return $time->format($format);
    }
    else {
      return NULL;
    }
  }
}
